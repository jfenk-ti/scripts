#!/bin/bash
#version 2
cd "$1"
revision="$2"

solution=$(ls | grep "^.*\.sln$" | grep -v "~" | head -n1)
if [ -z "$solution" ]
then
    echo "Could not find solution file (*.sln) in directory '$1'."
    echo "Are you shure you gave a valid directory?"
    echo "Aborting."
    exit 1
fi

/c/Windows/System32/taskkill.exe -F -IM "devenv.exe"
tasklist="bla"
while [ -n "$tasklist" ]; do
    tasklist=$(/c/Windows/System32/tasklist.exe | grep "devenv.exe")
done

git reset --hard

branches=($(git branch | tail -n +2))
for branch in "${branches[@]}"
do
    hash=$(git rev-parse "$branch")
    [[ "$revision" == "$hash" ]] && revision="$branch" && break
done

git checkout "$revision"
git.exe clean -d -x -f
cd "/C/Program Files (x86)/Microsoft Visual Studio/2017/Professional/Common7/IDE"
nohup ./devenv.exe "$1/$solution" &>/dev/null &